import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

declare var FruitasticApi:any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

allData       : any = [];
clonedAllData : any = [];
fruitArr      : any = [];
fruitsArr     : any = [];
clearFilterUI : Boolean = true
selectedFruit       = "";


constructor() { }

ngOnInit(): void {
// init9ial logic to get data as per the document    
FruitasticApi.get((data:any) => {
    this.allData = data;
    this.clonedAllData = data;
    this.getData()
})
}

// initial logic to populate the data
getData(){
    for (var data of this.allData) {
        if(this.fruitArr.indexOf(data.favoriteFruit) == -1){
            this.fruitArr.push(data.favoriteFruit);
            let fruitObj      = {} 
            fruitObj["name"]  = data.favoriteFruit;
            fruitObj["count"] = 1;
            this.fruitsArr.push(fruitObj);
        }  
        else{
            for (var index in this.fruitsArr){
                if(this.fruitsArr[index].name === data.favoriteFruit){
                let count=parseInt(this.fruitsArr[index].count)+1;
                this.fruitsArr[index].count = count;
                }
            };
        }
    }

    this.fruitArr =  this.fruitsArr.sort(this.sortByProperty("count"));
    this.fruitArr.reverse()
}

// sorting ascending method
 sortByProperty(property){  
    return function(a,b){  
       if(a[property] > b[property])  
          return 1;  
       else if(a[property] < b[property])  
          return -1;  
   
       return 0;  
    }  
 }

 // on cliclk of a fruit in top list
clickedFruit(fruit){  
    this.clearFilterUI = false 
    this.selectedFruit = "Fruit Name : " + fruit.name + " || Fruit Count : "  + fruit.count
    console.log("Fruit Selected", fruit.name + " " + fruit.count)
    this.clonedAllData = this.allData.filter(data => data.favoriteFruit == fruit.name);
}

//hide clear button and retrive data
clearFilter(){
    this.clonedAllData = this.allData;
    this.clearFilterUI = true 
}

}
